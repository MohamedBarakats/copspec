class Copspec < Formula
  desc "Runs rubocop and rspec/minitest on recently changed files"
  homepage "https://gitlab.com/MohamedBarakats/copspec"
  url "https://gitlab.com/MohamedBarakats/copspec/-/blob/main/copspec-v1.0.tar.gz"
  sha256 "4bf67e225f2ac08484d9d93f15b003249989f0a2111ed4801966b1f50822fabc"

  def install
    bin.install "copspec" => "copspec"
  end
end
